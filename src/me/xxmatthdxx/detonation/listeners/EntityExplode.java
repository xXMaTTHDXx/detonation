package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

/**
 * Created by Matt on 22/09/14.
 */
public class EntityExplode implements Listener {

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e){

        for(Block b : e.blockList()){
            BlockState s = b.getState();

            b.setType(Material.AIR);

            Detonation.getBlocks().add(s);
        }
    }
}
