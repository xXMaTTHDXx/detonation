package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import me.xxmatthdxx.detonation.ServerState;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

/**
 * Created by Matt on 21/09/14.
 */
public class ServerListPing implements Listener {

    @EventHandler
    public void onPing(ServerListPingEvent e){
        if(Detonation.getState() == ServerState.DISABLED){
            e.setMotd(ChatColor.RED + "" + ChatColor.BOLD + "Game is currently disabled!");
        }
        else if(Detonation.getState() == ServerState.LOBBY){
            e.setMotd(ChatColor.AQUA + "" + ChatColor.BOLD + "Jump into the game! We are in Lobby!");
        }
        else if(Detonation.getState() == ServerState.RESTARTING){
            e.setMotd(ChatColor.RED + "" + ChatColor.BOLD + "Game is RESTATING!");
        }
        else {
            e.setMotd(ChatColor.GOLD + "" + ChatColor.BOLD + "Game in progress....");
        }
    }
}
