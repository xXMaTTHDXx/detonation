package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import me.xxmatthdxx.detonation.ServerState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Matt on 20/09/14.
 */
public class BlockBreak implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e){

        if(Detonation.getState() == ServerState.DISABLED){
            e.setCancelled(false);
            return;
        }

        if(Detonation.getAlive().contains(e.getPlayer().getUniqueId()))
            e.setCancelled(true);

        if(Detonation.getSpectators().contains(e.getPlayer().getUniqueId()))
            e.setCancelled(true);
    }
}
