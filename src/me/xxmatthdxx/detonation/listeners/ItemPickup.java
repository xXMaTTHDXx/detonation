package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

/**
 * Created by Matt on 22/09/14.
 */
public class ItemPickup implements Listener {

    @EventHandler
    public void itemPickup(PlayerPickupItemEvent e){
        if(Detonation.getAlive().contains(e.getPlayer())){
            e.setCancelled(true);
        }
    }
}
