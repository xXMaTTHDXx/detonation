package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import me.xxmatthdxx.detonation.ServerState;
import me.xxmatthdxx.detonation.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * Created by Matt on 22/09/14.
 */
public class QuitEvent implements Listener {

    private static Detonation plugin = Detonation.plugin;

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {

        Player p = e.getPlayer();

        if (Detonation.getAlive().size() - 1 > 1) {
            Bukkit.broadcastMessage(MessageUtils.getLeaveMessage(e.getPlayer().getName()));
        } else if (Detonation.getAlive().size() == 1) {

            Bukkit.broadcastMessage(MessageUtils.getLeaveMessage(p.getName()));


        }

        for (UUID uuid : Detonation.getAlive()) {
            Bukkit.broadcastMessage(ChatColor.AQUA + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY + "" + ChatColor.BOLD
                    + Bukkit.getPlayer(uuid).getName() + ChatColor.GOLD + "" + ChatColor.BOLD + " Has won this round!");
        }

        Detonation.setState(ServerState.LOBBY);
        Detonation.setTicks(120);

        new BukkitRunnable() {

            public void run() {

                Detonation.getSpectators().clear();
                Detonation.getAlive().clear();

                for (Player pl : Bukkit.getOnlinePlayers()) {
                    for (PotionEffect pf : pl.getActivePotionEffects()) {
                        pl.removePotionEffect(pf.getType());
                    }

                    pl.setAllowFlight(false);
                    pl.setFlying(false);
                    Detonation.getAlive().add(pl.getUniqueId());
                    pl.teleport(Detonation.getLobbyLocation());
                }


            }
        }.runTaskLater(plugin, 200L);

        Detonation.updateMap();
    }
}
