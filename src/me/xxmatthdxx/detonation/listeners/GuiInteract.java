package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import me.xxmatthdxx.detonation.ServerState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matt on 22/09/14.
 */
public class GuiInteract implements Listener {

    Inventory inventory = Bukkit.createInventory(null, 9, "Class Selector");

    public void onInteract(PlayerInteractEvent e){
        if(Detonation.getState() != ServerState.LOBBY){
            return;
        }

        Player p = e.getPlayer();

        if(p.getItemInHand().getType() != Material.CHEST)
            return;

        ItemStack bomber = new ItemStack(Material.TNT, 1);
        ItemMeta bomberMeta = bomber.getItemMeta();
        bomberMeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Bomber");
        List<String> lore = new ArrayList<String>();

        if(p.hasPermission("kit.bomber")){
            lore.add(ChatColor.GOLD + "Shoots blocks of exploding TnT");
            lore.add(ChatColor.GOLD + "3 Second cooldown");
            lore.add(ChatColor.GOLD + "STATUS:" + ChatColor.GREEN + " Unlocked");

        }
        else {
            lore.add(ChatColor.RED + ">> LOCKED <<");
        }
        bomberMeta.setLore(lore);
        bomber.setItemMeta(bomberMeta);

        ItemStack striker = new ItemStack(Material.EMERALD, 1);
        ItemMeta strikerMeta = striker.getItemMeta();

        strikerMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Striker");
        List<String> strikerLore = new ArrayList<String>();

        if(p.hasPermission("kit.striker")){
            strikerLore.add("");
        }

        p.openInventory(inventory);
    }
}
