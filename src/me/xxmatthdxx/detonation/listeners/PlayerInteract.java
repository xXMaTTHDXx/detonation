package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Matt on 20/09/14.
 */
public class PlayerInteract implements Listener {

    private static Detonation plugin = Detonation.plugin;

    private ArrayList<UUID> cooldown = new ArrayList<UUID>();

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {

        final Player p = e.getPlayer();

        if(Detonation.getSpectators().contains(p.getUniqueId()))
            return;

        Action action = e.getAction();

        if (e.getPlayer().getItemInHand().getType() == Material.TNT) {
            if (action == Action.RIGHT_CLICK_AIR) {
                if (!cooldown.contains(e.getPlayer().getUniqueId())) {
                    Entity tnt = e.getPlayer().getWorld().spawn(e.getPlayer().getLocation(), TNTPrimed.class);
                    ((TNTPrimed) tnt).setFuseTicks(40);

                    tnt.setVelocity(e.getPlayer().getEyeLocation().getDirection().multiply(1.25));

                    tnt.setMetadata("thrower", new FixedMetadataValue(plugin, e.getPlayer().getName()));
                    cooldown.add(e.getPlayer().getUniqueId());

                    new BukkitRunnable(){
                        public void run(){
                            cooldown.remove(p.getUniqueId());
                        }
                    }.runTaskLater(plugin, 40L);
                }
                else {
                    p.getWorld().playSound(p.getLocation(), Sound.NOTE_PLING, 1, 1);
                }
            }
        }
    }
}
