package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import me.xxmatthdxx.detonation.ScoreboardUtils;
import me.xxmatthdxx.detonation.ServerState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.UUID;

/**
 * Created by Matt on 20/09/14.
 */
public class DeathListener implements Listener {

    private static Detonation plugin = Detonation.plugin;

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {

         final Player p = (Player) e.getEntity();

        if(Detonation.getState() == ServerState.LOBBY){
            p.setHealth(20);
            p.setFoodLevel(p.getFoodLevel());
            p.setVelocity(new Vector(0,0,0));
            p.teleport(Detonation.getLobbyLocation());
            return;
        }



        EntityDamageEvent.DamageCause cause = p.getLastDamageCause().getCause();

        if (cause == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION) {
            Entity tnt = p.getLastDamageCause().getEntity();

            List<MetadataValue> values = tnt.getMetadata("thrower");

            for (MetadataValue value : values) {
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    if (value.asString().equals(pl.getName())) {
                        Detonation.getKills().put(pl.getUniqueId(), Detonation.getKills().get(pl.getUniqueId()) + 1);
                        ScoreboardUtils.updateScore(pl);
                    }
                }
            }


            if (Detonation.getSpectators().contains(p.getUniqueId())) {
                return;
            }

            if (Detonation.getAlive().contains(p.getUniqueId())) {
                Detonation.getAlive().remove(p.getUniqueId());

                if (Detonation.getAlive().size() > 1) {
                    Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY + "" + ChatColor.BOLD
                            + p.getName() + ChatColor.GOLD + "" + ChatColor.GOLD + " has been " + ChatColor.RED + "" + ChatColor.BOLD
                            + " eliminated!");
                    p.removePotionEffect(PotionEffectType.JUMP);
                    p.removePotionEffect(PotionEffectType.SPEED);
                    p.getInventory().clear();
                    e.setDeathMessage(null);

                    p.setHealth(20);
                    p.setFireTicks(0);
                    p.setVelocity(new Vector(0, 0, 0));
                    p.teleport(Detonation.getMapSpawn());

                    p.setFlying(true);

                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.hidePlayer(p);

                    }
                    Detonation.getSpectators().add(p.getUniqueId());
                    return;

                } else if (Detonation.getAlive().size() == 1) {


                    p.setHealth(20L);
                    p.setFireTicks(0);
                    p.setAllowFlight(true);
                    p.setFlying(true);

                    for(Player pl : Bukkit.getOnlinePlayers()){
                        pl.showPlayer(p);
                        p.showPlayer(pl);
                    }

                    Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY + "" + ChatColor.BOLD
                            + p.getName() + ChatColor.GOLD + "" + ChatColor.GOLD + " has been " + ChatColor.RED + "" + ChatColor.BOLD
                            + " eliminated!");

                    for (UUID uuid : Detonation.getAlive()) {
                        Bukkit.broadcastMessage(ChatColor.AQUA + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY + "" + ChatColor.BOLD
                                + Bukkit.getPlayer(uuid).getName() + ChatColor.GOLD + "" + ChatColor.BOLD + " Has won this round!");
                    }

                    new BukkitRunnable() {

                        public void run() {

                            Detonation.getSpectators().clear();
                            Detonation.getAlive().clear();

                            for(Player pl : Bukkit.getOnlinePlayers()){
                                Detonation.getAlive().add(pl.getUniqueId());
                                pl.teleport(Detonation.getLobbyLocation());
                                pl.getInventory().clear();

                                for(PotionEffect pf : pl.getActivePotionEffects()){
                                    pl.removePotionEffect(pf.getType());
                                }

                                p.setFlying(false);
                                p.setAllowFlight(false);
                                pl.showPlayer(p);
                                p.showPlayer(pl);
                            }

                            Detonation.setState(ServerState.LOBBY);
                            Detonation.setTicks(120);
                        }
                    }.runTaskLater(plugin, 200L);

                    new BukkitRunnable(){
                        public void run(){
                            Detonation.updateMap();
                        }
                    }.runTaskLater(plugin, 200L);
                }
            }
        } else if(cause == EntityDamageEvent.DamageCause.FALL || cause == EntityDamageEvent.DamageCause.VOID){

            if (Detonation.getSpectators().contains(p.getUniqueId())) {
                return;
            }

            if (Detonation.getAlive().contains(p.getUniqueId())) {
                Detonation.getAlive().remove(p.getUniqueId());

                p.setHealth(20);
                p.setFireTicks(0);
                p.setVelocity(new Vector(0, 0, 0));



                if (Detonation.getAlive().size() > 1) {
                    Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY + "" + ChatColor.BOLD
                            + p.getName() + ChatColor.GOLD + "" + ChatColor.GOLD + " has been " + ChatColor.RED + "" + ChatColor.BOLD
                            + " eliminated!");
                    p.removePotionEffect(PotionEffectType.JUMP);
                    p.removePotionEffect(PotionEffectType.SPEED);
                    p.getInventory().clear();
                    e.setDeathMessage(null);

                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.hidePlayer(p);
                    }
                    p.teleport(Detonation.getLobbyLocation());
                    p.setAllowFlight(true);
                    p.setFlying(true);
                    Detonation.getSpectators().add(p.getUniqueId());

                } else if (Detonation.getAlive().size() == 1) {

                    Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY + "" + ChatColor.BOLD
                            + p.getName() + ChatColor.GOLD + "" + ChatColor.GOLD + " has been " + ChatColor.RED + "" + ChatColor.BOLD
                            + " eliminated!");

                    p.removePotionEffect(PotionEffectType.JUMP);
                    p.removePotionEffect(PotionEffectType.SPEED);
                    p.getInventory().clear();


                    p.setAllowFlight(true);
                    p.setFlying(true);
                    p.teleport(Detonation.getMapSpawn());

                    for (UUID uuid : Detonation.getAlive()) {
                        Bukkit.broadcastMessage(ChatColor.AQUA + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY + "" + ChatColor.BOLD
                                + Bukkit.getPlayer(uuid).getName() + ChatColor.GOLD + "" + ChatColor.BOLD + " Has won this round!");
                    }

                    new BukkitRunnable() {

                        public void run() {

                            Detonation.getSpectators().clear();
                            Detonation.getAlive().clear();

                            for(Player pl : Bukkit.getOnlinePlayers()){
                                Detonation.getAlive().add(pl.getUniqueId());
                                pl.teleport(Detonation.getLobbyLocation());
                                pl.showPlayer(p);
                                p.showPlayer(pl);
                                p.setAllowFlight(false);
                                p.setFlying(false);
                                pl.getInventory().clear();

                                for(PotionEffect pf : pl.getActivePotionEffects()){
                                    pl.removePotionEffect(pf.getType());
                                }
                            }

                            Detonation.setState(ServerState.LOBBY);
                            Detonation.setTicks(120);
                        }
                    }.runTaskLater(plugin, 200L);

                    new BukkitRunnable(){
                        @Override
                        public void run() {
                            Detonation.updateMap();
                        }
                    }.runTaskLater(plugin, 200L);
                }
            }
        }
    }
}
