package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import me.xxmatthdxx.detonation.ServerState;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * Created by Matt on 20/09/14.
 */
public class LoginListener implements Listener {

    private static Detonation plugin = Detonation.plugin;


    @EventHandler
    public void onJoin(PlayerLoginEvent e) {
        if (plugin.getState() == ServerState.DISABLED) {
            if (!e.getPlayer().isOp()) {
                e.disallow(PlayerLoginEvent.Result.KICK_OTHER, ChatColor.RED + "Server is Disabled!");
            }
            else {
                e.allow();
            }

        } else if (plugin.getState() == ServerState.STARTED) {
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, ChatColor.RED + "Game already started!");
        } else if (plugin.getState() == ServerState.RESTARTING) {
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, ChatColor.RED + "Server is restarting!");
        } else {
            e.allow();
        }
    }
}
