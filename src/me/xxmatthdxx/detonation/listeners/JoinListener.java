package me.xxmatthdxx.detonation.listeners;

import me.xxmatthdxx.detonation.Detonation;
import me.xxmatthdxx.detonation.ScoreboardUtils;
import me.xxmatthdxx.detonation.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Matt on 20/09/14.
 */
public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(MessageUtils.getJoinMessage(e.getPlayer().getName()));
        Detonation.getKills().put(e.getPlayer().getUniqueId(), 0);

        for (Player pl : Bukkit.getOnlinePlayers()) {
            ScoreboardUtils.updateScore(pl);
        }
        Detonation.getAlive().add(e.getPlayer().getUniqueId());
        e.getPlayer().getInventory().clear();

        e.getPlayer().teleport(Detonation.getLobbyLocation());

        ItemStack classSelector = new ItemStack(Material.CHEST, 1);
        ItemMeta classItemMeta = classSelector.getItemMeta();

        classItemMeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + " >> " + ChatColor.GOLD + "" +
        ChatColor.BOLD + "Class Selector" + ChatColor.AQUA + "" + ChatColor.BOLD + " << ");
        classSelector.setItemMeta(classItemMeta);

        e.getPlayer().getInventory().addItem(classSelector);
    }
}
