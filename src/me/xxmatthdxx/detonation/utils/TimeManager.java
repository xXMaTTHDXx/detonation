package me.xxmatthdxx.detonation.utils;

import me.xxmatthdxx.detonation.Detonation;
import me.xxmatthdxx.detonation.ServerState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * Created by Matt on 20/09/14.
 */
public class TimeManager implements Runnable {

    public final int LOBBY_SECONDS = 120;
    public final int ITEM_CLOCK = 20;
    public final int MIN_PLAYERS = 2;
    public final int RESTARTING_TIME = 120;
    public final int GAME_TIME = 260;

    public static final Detonation plugin = Detonation.plugin;

    @Override
    public void run() {

        if (Detonation.getTicks() > 0) {
            Detonation.setTicks(Detonation.getTicks() - 1);
        }

        if (Detonation.getState() == ServerState.LOBBY) {
        if ((Detonation.getTicks() > 0 && Detonation.getTicks() <= 10)
                || Detonation.getTicks() > 0 && Detonation.getTicks() % 30 == 0) {
            Bukkit.broadcastMessage(MessageUtils.getGameTag() + ChatColor.GOLD + "begins in " + ChatColor.AQUA + Detonation.formatTime(Detonation.getTicks()) + ChatColor.GOLD + "...");
        }



            int online = Bukkit.getServer().getOnlinePlayers().length;

            if (Detonation.getTicks() == 0) {
                if (online >= MIN_PLAYERS) {

                    for (UUID uuid : Detonation.getAlive()) {
                        Player p = Bukkit.getPlayer(uuid);
                        p.teleport(Detonation.getMapSpawn());
                    }

                    Detonation.setState(ServerState.STARTED);

                    Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + " >> " + ChatColor.AQUA + "" + ChatColor.BOLD + "Game Has Started!"
                            + ChatColor.GOLD + "" + ChatColor.BOLD + " <<");
                    for (Player p : Bukkit.getOnlinePlayers()) {

                        if (Detonation.getKit(p) == null) {
                            Detonation.getKit("Bomber").getUsers().add(p.getUniqueId());
                            p.getInventory().addItem(Detonation.getKit("Bomber").getItem());

                            p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 8));
                            p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));

                        }
                    }
                    Detonation.setTicks(GAME_TIME);

                } else {
                    Detonation.setTicks(LOBBY_SECONDS);
                    Bukkit.broadcastMessage(MessageUtils.getGameTag() + ChatColor.GOLD + "" + ChatColor.BOLD + "We need " + MIN_PLAYERS + " players in order to start the game!");
                }
            }
        } else if (Detonation.getState() == ServerState.STARTED) {

                 if(Detonation.getTicks() == 0){


                    Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY + "" + ChatColor.BOLD +
                            Bukkit.getPlayer(Detonation.getWinner()).getName() + ChatColor.AQUA + "" + ChatColor.BOLD +
                    " has won this round!");

                    new BukkitRunnable(){
                        public void run(){
                            for(Player pl : Bukkit.getOnlinePlayers()){
                                pl.teleport(Detonation.getLobbyLocation());
                            }
                        }
                    }.runTaskLater(plugin, 100);
                     Detonation.updateMap();
            }
        } else if (Detonation.getState() == ServerState.RESTARTING){
            if (Detonation.getTicks() <= 0) {
                Detonation.setState(ServerState.LOBBY);
                Detonation.setTicks(120);
            }
        }
    }
}
