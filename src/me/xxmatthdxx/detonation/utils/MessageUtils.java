package me.xxmatthdxx.detonation.utils;

import org.bukkit.ChatColor;

/**
 * Created by Matt on 20/09/14.
 */
public class MessageUtils {

    private static String gameTag = ChatColor.GRAY + "[" + ChatColor.DARK_RED + "" + ChatColor.BOLD + "Detonation" + ChatColor.GRAY + "] ";

    public static String getGameTag(){
        return gameTag;
    }

    public static String getJoinMessage(String playerName){
        return gameTag + ChatColor.GREEN + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY +
                "" + ChatColor.BOLD + playerName;
    }

    public static String getLeaveMessage(String playerName){
        return gameTag + ChatColor.DARK_RED + "" + ChatColor.BOLD + " >> " + ChatColor.GRAY +
                "" + ChatColor.BOLD + playerName;
    }
}
