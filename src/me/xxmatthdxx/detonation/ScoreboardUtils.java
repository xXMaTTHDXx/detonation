package me.xxmatthdxx.detonation;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

/**
 * Created by Matt on 21/09/14.
 */
public class ScoreboardUtils {

    public static void updateScore(Player p){
        ScoreboardManager manager = Bukkit.getScoreboardManager();

        Scoreboard scoreboard = manager.getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("test", "dummy");
        objective.setDisplayName(ChatColor.AQUA + "Leader Board");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        Score score = objective.getScore(p.getName());



            score.setScore(Detonation.getKills().get(p.getUniqueId()));
            p.setScoreboard(scoreboard);
        }
    }
