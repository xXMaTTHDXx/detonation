package me.xxmatthdxx.detonation.cmds;

import me.xxmatthdxx.detonation.Detonation;
import me.xxmatthdxx.detonation.ServerState;
import me.xxmatthdxx.detonation.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Matt on 20/09/14.
 */
public class CommandManager implements CommandExecutor {

    private static Detonation plugin = Detonation.plugin;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(command.getName().equalsIgnoreCase("gamespawn")){
            if(!(sender instanceof Player)){
                sender.sendMessage("Player only command");
                return true;
            }

            Player p = (Player) sender;
            Location loc = p.getLocation();

            plugin.getConfig().set("spawn.world", loc.getWorld().getName().trim());
            plugin.getConfig().set("spawn.x", loc.getX());
            plugin.getConfig().set("spawn.y", loc.getY());
            plugin.getConfig().set("spawn.z", loc.getZ());
            plugin.saveConfig();

            p.sendMessage(MessageUtils.getGameTag() + ChatColor.GOLD + "Spawn set!");


            return true;
        }
        else if(command.getName().equalsIgnoreCase("lobbyspawn")){
            if(!(sender instanceof Player)){
                sender.sendMessage("Player only command");
                return true;
            }

            Player p = (Player) sender;
            Location loc = p.getLocation();

            plugin.getConfig().set("lobby.world", loc.getWorld().getName().trim());
            plugin.getConfig().set("lobby.x", loc.getX());
            plugin.getConfig().set("lobby.y", loc.getY());
            plugin.getConfig().set("lobby.z", loc.getZ());
            plugin.saveConfig();

            p.sendMessage(MessageUtils.getGameTag() + ChatColor.GOLD + "Lobby spawn set!");
        }
        else if(command.getName().equalsIgnoreCase("disable")){
            Detonation.setState(ServerState.DISABLED);

            Detonation.setTicks(0);

            for(Player p : Bukkit.getOnlinePlayers()){
                if(p.isOp())
                    continue;
                p.kickPlayer(ChatColor.RED + "" + ChatColor.BOLD + "Admin has disabled this server! Come back later");
            }
            sender.sendMessage("Server disabled!");
        }
        return false;
    }

}
