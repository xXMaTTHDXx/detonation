package me.xxmatthdxx.detonation;

/**
 * Created by Matt on 20/09/14.
 */
public enum ServerState {

     RESTARTING, STARTED, DISABLED, LOBBY
}
