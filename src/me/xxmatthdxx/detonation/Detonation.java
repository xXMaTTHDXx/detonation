package me.xxmatthdxx.detonation;

import me.xxmatthdxx.detonation.cmds.CommandManager;
import me.xxmatthdxx.detonation.kits.Bomber;
import me.xxmatthdxx.detonation.kits.Kit;
import me.xxmatthdxx.detonation.listeners.*;
import me.xxmatthdxx.detonation.utils.TimeManager;
import org.bukkit.*;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.util.*;

/**
 * Created by Matt on 20/09/14.
 */
public class Detonation extends JavaPlugin {

    private static List<UUID> alive = new ArrayList<UUID>();
    private static List<UUID> spectators = new ArrayList<UUID>();
    private static ServerState state;

    private static List<BlockState>blocks = new ArrayList<BlockState>();
    private static HashMap<UUID, Integer> kills = new HashMap<UUID, Integer>();
    private static List<Kit> kits = new ArrayList<Kit>();

    private static int ticks = 0;

    public static Detonation plugin;

    /**
     * TODO List
     * Tnt only blows up certain items
     * Send players to hub
     * Create sign system
     * create more classes
     * create class selectors
     */

    public void onEnable(){
        plugin = this;


        state = ServerState.RESTARTING;

        CommandManager cm = new CommandManager();

        getCommand("gamespawn").setExecutor(cm);
        getCommand("lobbyspawn").setExecutor(cm);
        getCommand("disable").setExecutor(cm);

        if(getConfig() == null){
            saveDefaultConfig();
        }

        else {
            saveConfig();
        }

        if(spectators.size() > 0){
            for(UUID uuid : spectators){
                Player p = Bukkit.getPlayer(uuid);
                p.performCommand("spawn");
                p.setHealth(20L);
                p.setFireTicks(0);
                p.setFoodLevel(20);
            }
        }

        if(alive.size() > 0){
            for(UUID uuid : alive){
                Player p = Bukkit.getPlayer(uuid);
                p.performCommand("spawn");
                p.setHealth(20L);
                p.setFireTicks(0);
                p.setFoodLevel(20);
                p.setVelocity(new Vector(0,0,0));
            }
        }

        Bukkit.broadcastMessage(ChatColor.RED + "RESTARTING!");
        spectators.clear();
        alive.clear();

        kits.add(new Bomber());

        Bukkit.getServer().getScheduler().runTaskTimer(this, new TimeManager(), 20L, 20L);

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new JoinListener(), this);
        pm.registerEvents(new LoginListener(), this);
        pm.registerEvents(new DeathListener(), this);
        pm.registerEvents(new BlockBreak(), this);
        pm.registerEvents(new PlayerInteract(), this);
        pm.registerEvents(new FoodLevelChange(), this);
        pm.registerEvents(new ServerListPing(), this);
        pm.registerEvents(new QuitEvent(), this);
        pm.registerEvents(new ItemPickup(), this);
        pm.registerEvents(new EntityExplode(), this);


        state = ServerState.LOBBY;
        ticks = 120;
    }

    public void onDisable(){
        getServer().getScheduler().cancelTasks(plugin);
    }

    public static ServerState getState(){
        return state;
    }

    public static int getTicks() { return ticks; }

    public static void setTicks(int t){
        ticks = t;
    }

    public static String formatTime(int seconds){

        int hours = seconds / 3600;
        int remainder = seconds % 3600;
        int minutes = remainder / 60;
        int sec = remainder%60;

        if(hours > 0){
            return hours + ":" + minutes + ":" + sec;
        }
        else if(minutes > 0){
            return minutes + ":" + sec;
        }
        else {
            return String.valueOf(sec);
        }
    }

    public static void setState(ServerState s){
        state = s;
    }

    public static Kit getKit(Player p){
        for(Kit k : kits){
            if(k.getUsers().contains(p.getUniqueId())){
                return k;
            }
        }
        return null;
    }

    public static Kit getKit(String name){
        for(Kit kit : kits){
            if(kit.getName().equals(name)){
                return kit;
            }
        }
        return null;
    }

    public static List<UUID> getAlive(){
        return alive;
    }

    public static List<UUID> getSpectators(){
        return spectators;
    }

    public static Location getMapSpawn(){
        World world = Bukkit.getWorld(plugin.getConfig().getString("spawn.world"));
        int x = plugin.getConfig().getInt("spawn.x");
        int y = plugin.getConfig().getInt("spawn.y");
        int z = plugin.getConfig().getInt("spawn.z");

        return new Location(world, x, y, z);
    }

    public static List<BlockState> getBlocks(){
        return blocks;
    }

    public static HashMap<UUID, Integer> getKills(){
        return kills;
    }

    public static UUID getWinner(){
        int highest = Collections.max(kills.values());

        for(UUID uuid : kills.keySet()){
            if(kills.get(uuid) == highest){
                return uuid;
            }
        }
        return null;
    }
    public static Location getLobbyLocation(){
        World world = Bukkit.getWorld(plugin.getConfig().getString("lobby.world"));
        int x = plugin.getConfig().getInt("lobby.x");
        int y = plugin.getConfig().getInt("lobby.y");
        int z = plugin.getConfig().getInt("lobby.z");
        return new Location(world, x, y, z);
    }

    public static void updateMap(){
        for(BlockState blockState : blocks){
            blockState.update(true);
        }
    }
}
