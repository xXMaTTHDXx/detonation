package me.xxmatthdxx.detonation.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Matt on 20/09/14.
 */
public class Bomber implements Kit {

    @Override
    public ItemStack getItem() {
        return new ItemStack(Material.TNT);
    }

    @Override
    public String getName() {
        return "Bomber";
    }

    @Override
    public List<UUID> getUsers() {
        return new ArrayList<UUID>();
    }
}
