package me.xxmatthdxx.detonation.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Matt on 22/09/14.
 */
public class Striker implements Kit {
    @Override
    public ItemStack getItem() {
        return new ItemStack(Material.EMERALD, 1);
    }

    @Override
    public String getName() {
        return "Striker";
    }

    @Override
    public List<UUID> getUsers() {
        return new ArrayList<UUID>();
    }
}
