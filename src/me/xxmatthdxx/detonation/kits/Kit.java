package me.xxmatthdxx.detonation.kits;

import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;

/**
 * Created by Matt on 20/09/14.
 */
public interface Kit {

    public ItemStack getItem();

    public String getName();

    public List<UUID> getUsers();

}
